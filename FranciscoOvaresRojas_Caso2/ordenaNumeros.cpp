/**
 * Algoritmo de lista de nodos Playeres.
 * @author Francisco Javier Ovares Rojas.
 * @date 16/09/2021
*/

#include <conio.h>
#include "tools/ordain.h"

int main(){
    // Float list creation 0.
    float numbers0[] = {10, 17, 5, 4, 7, 9, 21, 56, 39};
    int listSize0 = (int)sizeof(numbers0)/sizeof(numbers0[0]);
    float* firstPointer = numbers0;
    cout<<"Size of List -> "<<listSize0<<endl;
    // Ordain the list.
    printList(firstPointer, listSize0);
    sortBySelectionSort(firstPointer, listSize0);
    printList(numbers0, listSize0);
    cout<<endl;
    
    // Float list creation 1.
    float numbers1[] = {5, 4, 7, 9, 21, 56, 39};
    int listSize1 = (int)sizeof(numbers1)/sizeof(numbers1[0]);
    float *secondPointer = numbers1;
    cout<<"Size of List -> "<<listSize1<<endl;
    // Ordain the list.
    printList(secondPointer, listSize1);
    sortBySelectionSort(secondPointer, listSize1);
    printList(numbers1, listSize1);
    cout<<endl;

    // Float list creation 2.
    float numbers2[] = {5, 4, 6, 1, 3};
    int listSize2 = (int)sizeof(numbers2)/sizeof(numbers2[0]);
    float *thirdPointer = numbers2;
    cout<<"Size of List -> "<<listSize2<<endl;
    // Ordain the list.
    printList(thirdPointer, listSize2);
    sortBySelectionSort(thirdPointer, listSize2);
    printList(numbers2, listSize2);
    cout<<endl;

    // System Pause.
    getch();
    return 0;
}