#ifndef TEAM 
#define TEAM 1
#define SIZE_TEAM 24
#define END 25

#include "player.h"
#include <string>

struct TeamList{
    int quantity;
    Player *teamList = NULL;

    bool isEmpty(){
        return quantity > 0;
    }

    int getQuantity(){
        return quantity;
    }

    void addPlayer(int pNumber, string pName) {
        /**Player *newPlayer = new Player(pNumber, pName);
        newPlayer->next = teamList;
        teamList = newPlayer;
        quantity += 1;
        */
        insertPlayer(pNumber, pName, END); 
    }

    Player *getPlayerbyIndex(int pIndex) {
        Player *tempPlayer = NULL;

        for (Player *temp = teamList; temp != NULL; temp=temp->next) {
            if ( temp->index == pIndex){
                tempPlayer = temp;
            }   
        }

        return tempPlayer;
    }

    void listPlayers(){    
        if (teamList != NULL){
            for (Player *temp = teamList; temp != NULL; temp=temp->next) {
                cout<<"Number: "<<temp->number<<"\tName: "<<temp->name<<endl;
            }
        }
    }

    bool findPlayerByName(string pName){
        bool isPlayer = false;
        for (Player *temp = teamList; temp != NULL; temp = temp->next){
            if (temp->name.compare(pName) == 0){
                isPlayer = true;
                break;
            }
        }
        
        return isPlayer;
    }

    bool findPlayerByNumber(int pNumber){
        bool isPlayer = false;
        for (Player *temp = teamList; temp != NULL; temp = temp->next){
            if (temp->number == pNumber){
                isPlayer = true;
                break;
            }
        }

        return isPlayer;
    }

    void _resetIndexPlayers(){
        int index  = 0;
        for (Player *temp = teamList; temp != NULL; temp = temp->next){
            temp->index = index;
            index++;
        }
    }

    /** 
    * Remove a player based on a supplied T-shirt number.
      * Case if: The entire list has been traversed and the player does not exist.
      * Else if case: The player to be eliminated is the first in the list.
      * Else case: The player to be eliminated is not the first on the list.
      * @param pNumber Represents the shirt number.
      * @return isRemoved Returns true if the player was removed otherwise it returns false.
    */
    bool removePlayer(int pNumber){
        bool isRemoved;
        Player *playerTemp = teamList;
        Player *beforePlayer = NULL;

        // The position of the player to be removed is located.
        while ( (playerTemp != NULL) && (playerTemp->number != pNumber) ) {
            beforePlayer = playerTemp;
            playerTemp = playerTemp->next;
        }

        if (playerTemp == NULL){
            isRemoved = false;
        }
        else if (beforePlayer == NULL){ 
            teamList = teamList->next;
            delete playerTemp;
            isRemoved = true;
        }
        else{
            beforePlayer->next = playerTemp->next;
            delete playerTemp;
            isRemoved = true;
        }
        quantity -= 1;
        _resetIndexPlayers();
        
        return isRemoved;
    }

    void removeByIndex(int pIndex){
        Player *playerTemp = teamList;
        Player *beforePlayer = NULL;

        // The position of the player to be removed is located.
        while ( (playerTemp != NULL) && (playerTemp->index != pIndex) ) {
            beforePlayer = playerTemp;
            playerTemp = playerTemp->next;
        }

        if (playerTemp == NULL){
            ;
        }
        else if (beforePlayer == NULL){ 
            teamList = teamList->next;
            delete playerTemp;
            
        }
        else{
            beforePlayer->next = playerTemp->next;
            delete playerTemp;
            
        }
        quantity -= 1;
        _resetIndexPlayers();
       
    }

    void intercambiatePlayers(Player *pOne, Player *pTwo) {
        int indexOne = pOne->index;
        int indexTwo = pTwo->index;

        Player *tempPlayerOne = new Player(pOne->number, pOne->name);
        Player *tempPlayerTwo = new Player(pTwo->number, pTwo->name);
        tempPlayerOne->index = indexTwo;
        tempPlayerTwo->index = indexOne;

        //cout<<"Players a intercambiar -> "<<pOne->name<<" <-> "<<pTwo->name<<endl;
        
        removeByIndex(indexOne);
        insertP(tempPlayerTwo->number, tempPlayerTwo->name, indexOne);
        removeByIndex(indexTwo);
        insertP(tempPlayerOne->number, tempPlayerOne->name, tempPlayerOne->index);
        
        
    }

    /** 
     * Insert a player in the desired index, if the index exceeds it is simply placed at the end.
     * At the end of the case, the instruction to enter the newPlayer's *next pointer is always executed.
     * If case: The while was not entered, therefore the player is placed at the zero index.
     * Else case: It is placed in the requested index or at the end simply if it does not exist.
     * @param pNumber Represents the shirt number.
     * @param pName Represents the name of the player.
     * @param pPosition Represents the index where you want to insert the player.
    */
    void insertP(int pNumber, string pName, int pPosition){
        Player *newPlayer = new Player(pNumber, pName);
        int cont = 0;
        // Auxiliary nodes for player list order.
        Player *playerTemp = teamList;
        Player *beforePlayer = NULL;
        // The position to be inserted is located.
        while ( (playerTemp != NULL) && (cont < pPosition) ){
            beforePlayer = playerTemp;
            playerTemp = playerTemp->next;
            cont++;
        }
        
        if (playerTemp == teamList){
            teamList = newPlayer;
        }
        else{
            beforePlayer->next = newPlayer;
        }
        newPlayer->next = playerTemp;
        // Adding to the counter 1 player.
        quantity += 1;
        _resetIndexPlayers();
    }

    // Validate if the number or name exists.
    void insertPlayer(int pNumber, string pName, int pPosition){
        if ((findPlayerByNumber(pNumber) == true) || (findPlayerByName(pName) == true)){
           cout<<"Number/Name alredy exist. Input another."<<endl;
        }
        else{
            insertP(pNumber, pName, pPosition);
        }
    }

};

#endif