#ifndef PLAYER
#define PLAYER 1

#include <iostream>

using namespace std;

int indexPlayer(){
 	static int index = 0;
 	index++;
    return index;
}

struct Player {
    // Attributes.
    int index;
    int number;
    string name;
    Player *next = NULL;

    // Builder.
    Player (int pNumber, string pName){
        number = pNumber;
        name = pName;
        next = NULL;
        index = indexPlayer();
    }
};

#endif