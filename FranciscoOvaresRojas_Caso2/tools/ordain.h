#ifndef ORDAIN 
#define ORDAIN 1

#include <iostream>

using namespace std;

void sortBySelectionSort(float *pList, int arraySize){
    int min; 
    float auxNum;

    for(int i = 0; i <= arraySize-1; i++){
        min = i;
        for(int j = i+1; j < arraySize; j++){
            //cout<<"Numeros comparados: "<<pList[j]<<" < "<<pList[i]<<endl;
            if(pList[j] < pList[i]){
                min = j;
                auxNum = pList[i];
                pList[i] = pList[min];
                pList[min] = auxNum;
            }
        }
        
    }
}

void printList(float *pList, int arraySize) {

    cout<<"List -> ";
    for (int mainIndex = 0; mainIndex <= arraySize-1; mainIndex++) {
        cout<<pList[mainIndex]<<"\t";
    }
    cout<<endl;
}

#endif