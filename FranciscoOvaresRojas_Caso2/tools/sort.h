#ifndef SORT 
#define SORT 1

#include "teamList.h"
#include "player.h"
#include <cstring>

void sortByInsertionSort(TeamList *pTeam, int pQuantityOfPlayers){
    int position1 = 0, position2 = 0;
    char first, second;
    Player *tempPlayer = pTeam->teamList;
    Player *tempPlayer2 = NULL;
    Player *tempPlayer3 = NULL;

    // Insertion algorithm.
    for (int i = 0; i < pQuantityOfPlayers; i++) {
        tempPlayer2 = pTeam->getPlayerbyIndex(i);

        for (int j = 0; j < i; j++) {
            tempPlayer3 = pTeam->getPlayerbyIndex(j);
            //cout<<tempPlayer2->name<<"\t";
            //cout<<tempPlayer3->name<<endl;
            if (tempPlayer3->name[0] == tempPlayer2->name[0]) {
                continue;
            }
            else if (tempPlayer3->name[0] > tempPlayer2->name[0]){
                //cout<<"Players a intercambiar -> "<<tempPlayer2->name<<"\t"<<" <-> "<<tempPlayer3->name<<endl;
                pTeam->intercambiatePlayers(tempPlayer2, tempPlayer3);
                //pTeam->listPlayers();
            }
            else{
                continue;
            }
        }
    }
    
}

#endif