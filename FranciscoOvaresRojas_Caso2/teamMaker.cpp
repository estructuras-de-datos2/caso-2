/**
 * Players node list algorithm.
 * @author Francisco Javier Ovares Rojas.
 * @date 16/09/2021
*/

#include <iostream>
#include <conio.h>
#include "tools/teamList.h"
#include "tools/sort.h"

using namespace std;

int main(){
    // Creating the list.
    TeamList *CostaRica = new TeamList();

    // Adding players.
    CostaRica->addPlayer(11,"Francisco");
    CostaRica->addPlayer(10,"Ronaldo");
    CostaRica->addPlayer(9,"Elías");
    CostaRica->addPlayer(3,"Alejandro");
    CostaRica->addPlayer(8,"Anatoley");

    // Valuing if it´s empty.
    bool a = CostaRica->isEmpty();
    if (a == true){
        cout<<"Lista vacía."<<endl;
    }
    else{
        cout<<"Lista no vacía."<<endl;
    }
    // Consulting number of players.
    int quantity = CostaRica->getQuantity();
    if (quantity == 0){
        cout<<"Hay "<<quantity<<" jugadores."<<endl;
    }
    else{
        cout<<"Hay "<<quantity<<" jugadores."<<endl;
    }

    // Deleting (T-shirt number).
    cout<<"Datos sin jugares eliminados."<<endl;
    CostaRica->listPlayers();
    CostaRica->removePlayer(10);
    cout<<"Datos con jugares eliminados."<<endl;
    CostaRica->listPlayers();
    cout<<endl<<endl;

    // Inserting (T-shirt number / Name / Index).
    cout<<"Datos sin jugares insertados."<<endl;
    CostaRica->listPlayers();
    CostaRica->insertPlayer(5, "Luis", 0);
    CostaRica->insertPlayer(17, "Keilor", 0);
    CostaRica->insertPlayer(7, "Ronaldo", 1);
    CostaRica->insertPlayer(1, "Kendall", 20);
    CostaRica->insertPlayer(1, "Kendall", 21);  // It will show that the name / number already exists
    CostaRica->insertPlayer(1, "Kevin", 20);    // It will show that the name / number already exists
    cout<<"Datos con jugares insertados"<<endl;
    CostaRica->listPlayers();
    cout<<endl<<endl;
    //CostaRica->intercambiatePlayers(CostaRica->teamList->next->next, CostaRica->teamList->next->next->next->next);
    // Sorting the list of players.
    /**char ar = CostaRica->teamList->name[0];
    char br = CostaRica->teamList->next->name[0];
    if (ar < br){
        cout<<"a ->"<<ar<<" es menor alfabeticamente que b->"<<br<<endl;
    }*/
    TeamList *teamPointer = CostaRica;
    sortByInsertionSort(teamPointer, CostaRica->getQuantity());
    cout<<"Datos con jugares ordenados alfabéticamente."<<endl;
    CostaRica->listPlayers();
    cout<<endl<<endl;

    // System Pause.
    getch();
    return 0;
}
